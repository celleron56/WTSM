import pygame
import math
import random
import time 
random.seed(21)
pygame.init()  #Initialize

#====================INITIALIZING ===================================

scr_X = 300
scr_Y = 400

screen = pygame.display.set_mode((scr_X,scr_Y))  # ( X, Y )

# ========================== (R,G,B) ==========================

red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
black = (0,0,0)
white = (255,255,255)
grey = (128,128,128)
yellow = (255,255,0)
class circle():
    def __init__(self,x,y,size):
        self.static = False
        self.x = x
        self.y = y
        self.color = random.randint(1,10)
        self.size = size
        self.v2d = [0,-1]
    def run(self, othercircles , screen):
        self.x += self.v2d[0]
        self.y += self.v2d[1]
        for i in range(0,len(othercircles) -1 ):
           if not othercircles[i] == self :
             othercircles[i] = self.collisioncheck(othercircles[i])
        if self.x  < 0 :
            self.x = 0
        if self.y < 0 :
            self.y = 0
        if self.x  > 300 :
            self.x = 300
        if self.y > 400 :
            self.y = 400
        if self.v2d[1] < 1 :
            self.v2d[1] += 4
        pygame.draw.circle(screen, [2,32,((300 - self.y) * 0.1) + 200 + self.color] ,[self.x,self.y],self.size)
        return [othercircles, screen]
    def changevector(self,x,y):
        self.v2d = [x,y]
    def collisioncheck(self,circle):
        distance_between = math.sqrt((self.x -circle.x)**2+(self.y- circle.y)**2)
        if(distance_between <=self.size+circle.size ):
            ratio = 0.5 #ratio between how much 2 circles are affected by pushes between them
            if self.y < circle.y :
                ratio = 0.25
            elif self.y == circle.y:
                ratio = 0.5
            else:
                ratio = 0.75
            if circle.static == True :
                ratio = 1
            
            ###endif
            circle_distx = (self.x - circle.x)
            circle_disty = (self.y - circle.y)
            
            self.x = self.x + (circle_distx * ratio)
            self.y = self.y + (circle_disty * ratio)
            self.changevector(circle_distx * 0.2 , circle_disty * 0.2 + (random.randint(-10,10) * 0.1))
            circle.changevector(circle_distx * -0.2 , circle_disty * -0.2 + (random.randint(-10,10) * 0.1))
            circle.x = circle.x - ( circle_distx * (1 - ratio ))
            circle.y = circle.y - ( circle_disty * (1 - ratio ))
        return circle
            
        


gameOver = False
circle_list = []
for i in range(1,200):
    circle_list.append(circle(random.randint(0,100),random.randint(0,100),10))


while not gameOver:
    # ========================Event Loop==================4===4
    for event in pygame.event.get():
        #print (event)
        if event.type == pygame.QUIT:
            gameOver = True
    
    #=========================Game Logic=====================

    
    for i in circle_list:
       t = i.run(circle_list,screen)
       screen = t[1]
       circle_list = t[0]
    mousepos = pygame.mouse.get_pos()
    mouse_x = mousepos[0]  ## note read as mouse (DASH) x pls
    mouse_y = mousepos[1]
    cursor = circle(mouse_x , mouse_y, 20)
    cursor.static = True
    for i in circle_list:
      i.collisioncheck(cursor)
        
    time.sleep(0.05)
    pygame.display.flip()  #update()
    screen.fill([200,200,200])
    
pygame.quit()
quit()









